const express = require("express");
const app = express();
const routes = require("./routes");

app.use(routes);

process.on("unhandledRejection", function(err) {
  console.log("UnhandledRejection error" + err);
});
process.on("uncaughtException", function(error) {
  console.log("UncaughtException" + error);
  console.log("Error Stack", error.stack);
});

app.listen(process.env.PORT || 3000, () => {
  console.log(`listening on port ${process.env.PORT || 3000}!`);
});
