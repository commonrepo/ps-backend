const dbo = require("../db");

function store(req, res) {
  const page = req.query.page;
  const limit = req.query.limit || 20;
  dbo.then(async db => {
    const skip = page > 1 ? page * (limit - 1) : 0;
    const getAppsData = await db
      .collection("apps")
      .find()
      .skip(skip)
      .limit(limit)
      .toArray();
    const getAppsCount = await db.collection("apps").countDocuments();
    res.status(200).send({
      data: getAppsData,
      count: Math.ceil(getAppsCount / limit)
    });
  });
}

module.exports = store;
