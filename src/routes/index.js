const app = (module.exports = require("express")());
const store = require("../controllers/store");
const appDetail = require("../controllers/appDetail");
const forceUpdate = require("../controllers/forceUpdate");
const scrapData = require("../controllers/scrapData");

app.get("/api/getAll", store);

app.get("/api/getById", appDetail);

app.get("/api/update", forceUpdate);

app.get("/api/scrap", scrapData);
