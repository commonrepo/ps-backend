const gplay = require("google-play-scraper");
const dbo = require("../db");

exports.handleAppsFetch = async function() {
  await gplay
    .list({
      collection: gplay.collection.TOP_FREE,
      sort: gplay.sort.NEWEST
    })
    .then(async allApps => {
      await dbo.then(
        async db =>
          await db
            .collection("apps")
            .insertMany(allApps, function(err, response) {
              if (err) throw err;
              allApps.map(v => {
                gplay.app({ appId: v.appId }).then(appDetails => {
                  db.collection("appsInfo").insertOne(appDetails, function(
                    appErr,
                    appResponse
                  ) {
                    if (appErr) throw appErr;
                    console.log(
                      "Number of document inserted: " +
                        appResponse.insertedCount
                    );
                  });
                });
              });
              console.log(
                "Number of documents inserted: " + response.insertedCount
              );
            })
      );
    });
  return true;
};
exports.handleAppsUpdate = function() {
  gplay
    .list({
      collection: gplay.collection.TOP_FREE,
      sort: gplay.sort.NEWEST
    })
    .then(allApps => {
      allApps.map(singleApp =>
        dbo.then(db => {
          db.collection("apps").findOne({ appId: singleApp.appId }, function(
            appErr,
            appRes
          ) {
            if (appErr) throw appErr;
            if (!appRes) {
              db.collection("apps").insertOne(singleApp, function(
                insertErr,
                insertRes
              ) {
                if (insertErr) throw insertErr;
                gplay.app({ appId: singleApp.appId }).then(appDetails => {
                  db.collection("appsInfo").insertOne(appDetails, function(
                    appError,
                    appResponse
                  ) {
                    if (appError) throw appError;
                    console.log(
                      "Number of document inserted: " +
                        appResponse.insertedCount
                    );
                  });
                });
              });
            }
          });
        })
      );
    });
};
